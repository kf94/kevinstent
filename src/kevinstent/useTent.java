/*This object will hold information about
 *Programmer: Kevin Fegan
 *Date: 15/3/2015
 */

import java.util.Calendar;
import java.util.Scanner;

class useTent {

    //global variable

    static Scanner keyboard = new Scanner(System.in);

    public static void main(String[] args) {
        //Tent instances
        Tent tentA;
        Tent tentB;
        Tent tentC;

        //HireContract instances
        HireContract contractA;
        HireContract contractB;
        HireContract contractC;

        Calendar today = Calendar.getInstance();

        tentA = new Tent("ABC123", "Big Tent", 10.5, 11.5, 12.0, 30);
        tentB = new Tent("DEF456", "Medium Tent", 7.25, 8.5, 9.5, 15);
        tentC = new Tent("GHI789", "Small Tent", 5.0, 6.25, 7.0, 10);

        contractA = new HireContract();
        contractB = new HireContract();
        contractC = new HireContract();

        processHire();
        processHire();
    }

    public static void processHire() {
        System.out.print("cniRef: ");
        String uniRef = keyboard.nextLine();
        keyboard.next();
        
        System.out.println("custRef: ");
        String custRef = keyboard.nextLine();
        keyboard.next();
        
        System.out.println("dayHireRate: ");
        double dayHireRate = keyboard.nextDouble();
        keyboard.next();
        
        System.out.println("startDate: ");
        System.out.println("Year: ");
        int year = keyboard.nextInt();
        System.out.println("Month: ");
        int month = keyboard.nextInt();
        System.out.println("Day: ");
        int day = keyboard.nextInt();
        Calendar startDate = Calendar.getInstance();
        startDate.set(year, month, day);
        
        System.out.println("endDate: ");
        System.out.println("Year: ");
        year = keyboard.nextInt();
        System.out.println("Month: ");
        month = keyboard.nextInt();
        System.out.println("Day: ");
        day = keyboard.nextInt();
        Calendar endDate = Calendar.getInstance();
        endDate.set(year, month, day);
        
        System.out.println("tentHired: ");
        //Tent tentHired = keyboard.nextLine();
        //keyboard.next();
        
    }

    public static void terminateHireContract() {

    }
}
