/*This class will hold information about
 *Programmer: Kevin Fegan
 *Date: 15/3/2015
 */
import java.util.Calendar;
class Tent
{
 		//variables
 	private String ref;
 	private String modelName;
 	private double width, height, depth;
 	private int buildTime;
 	private long totalDaysHired;
 	private Calendar lastReturn;
 	private boolean availability;
 	private HireContract recentContract;

		//Constructors
 	public Tent()
 	{
 		ref = "";
 		modelName = "";
 		width = 0.00;
 		height = 0.00;
 		depth = 0.00;
 		buildTime = 0;
 		totalDaysHired = 0;
 		lastReturn = null;
 		availability = false;
 		recentContract = null;
 	}

 	public Tent(String ref, String modelName, double width, double height, double depth, int buildTime)
 	{
 		this.ref = ref;
 		this.modelName = modelName;
 		this.width = width;
 		this.height = height;
 		this.depth = depth;
 		this.buildTime = buildTime;
 		this.totalDaysHired = totalDaysHired;
 		this.lastReturn = lastReturn;
 		this.availability = availability;
 		this.recentContract = recentContract;
 	}
		//set Methods
 	public void setRef(String ref)
 	{
 		this.ref = ref;
 	}

 	public void setModelName(String modelName)
 	{
 		this.modelName = modelName;
 	}

 	public void setWidth(double width)
 	{
 		this.width = width;
 	}

 	public void setHeight(double height)
 	{
 		this.height = height;
 	}

 	public void setDepth(double depth)
 	{
 		this.depth = depth;
 	}

 	public void setBuildTime(int buildTime)
 	{
 		this.buildTime = buildTime;
 	}

 	public void setTotalDaysHired(long totalDaysHired)
 	{
 		this.totalDaysHired = totalDaysHired;
 	}

 	public void setLastReturn(Calendar lastReturn)
 	{
 		this.lastReturn = lastReturn;
 	}

 	public void setAvailability(boolean availability)
 	{
 		this.availability = availability;
 	}

 	public void setRecentContract(HireContract recentContract)
 	{
 		this.recentContract = recentContract;
 	}

		//get Methods
 	public String getRef()
 	{
 		return ref;
 	}

 	public String getModelName()
 	{
 		return modelName;
 	}

 	public double getWidth()
 	{
 		return width;
 	}

 	public double getHeight()
 	{
 		return height;
 	}

 	public double getDepth()
 	{
 		return depth;
 	}

 	public int getBuildTime()
 	{
 		return buildTime;
 	}

 	public long getTotalDaysHired()
 	{
 		return totalDaysHired;
 	}

 	public Calendar getLastReturn()
 	{
 		return lastReturn;
 	}

 	public boolean getAvailability()
 	{
 		return availability;
 	}

 	public HireContract getRecentContract()
 	{
 		return recentContract;
 	}

		//takes 2 date values and uses them to calculate total days hired
 	public void addDaysHired(Calendar startDate, Calendar endDate)
 	{
 			//calendar object created using getInstance() method
 		Calendar today = Calendar.getInstance();

 		long t1 = startDate.getTimeInMillis();
 		long t2 = endDate.getTimeInMillis();
 		long total;

 		total = t2 - t1;

			//total from milliseconds to days
 		totalDaysHired = total/1000/60/60/60/24;
 	}
 		//takes 2 date values and uses them to update total days hired
 	public void returnTent(Calendar startDate, Calendar endDate)
 	{
 		Calendar today = Calendar.getInstance();

			//last return date updated,tent made available for hire again
 		lastReturn = endDate;
 		availability = true;

 		long t1 = startDate.getTimeInMillis();
 		long t2 = endDate.getTimeInMillis();
 		long total;

			//takes end date from start date to give total time in milliseconds
 		total = t2 - t1;

			//converts total from milliseconds to days
 		total = total/1000/60/60/60/24;
		totalDaysHired = total;
 	}

 		//output Tent data and latest reference number for HireContract
 	public String toString()
 	{
 		String s = "";

		s = s + "\n";
		s = s + "**********TENT INFORMATION**********" + "\n";
 		s = s + "Unique reference: " + ref + "\n";
 		s = s + "Model name: " + modelName + "\n";
 		s = s + "\n";

 		s = s + "**********TENT DIMENSIONS**********" + "\n";
 		s = s + "Width: " + width + "ft" + "\n";
 		s = s + "Height: " + height + "ft" + "\n";
 		s = s + "Depth: " + depth + "ft" + "\n";
 		s = s + "\n";

 		s = s + "**********OTHER INFORMATION**********" + "\n";
 		s = s + "Available?: " + availability + "\n";
 		s = s + "Most recent hire contract: " + recentContract + "\n";

 		return s;
 	}

}


