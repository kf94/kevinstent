/*This object will hold information about
 *Programmer: Kevin Fegan
 *Date: 15/3/2015
 */
import java.util.Calendar;
class HireContract
{
		//variables
 	private String uniRef;
 	private String custRef;
 	private double dayHireRate;
 	private Calendar startDate;
 	private Calendar endDate;
 	private Tent tentHired;

 		//Constructors
 	public HireContract()
 	{
 		uniRef = "";
 		custRef = "";
 		dayHireRate = 0.00;
 		startDate = null;
 		endDate = null;
 		tentHired = null;
 	}

 	public HireContract(String uniRef, String custRef, double dayHireRate, Calendar startDate, Calendar endDate, Tent tentHired)
 	{
 		this.uniRef = uniRef;
 		this.custRef = custRef;
 		this.dayHireRate = dayHireRate;
 		this.startDate = startDate;
 		this.endDate = endDate;
 		this.tentHired = tentHired;
 	}

 		//set Methods
 	public void setUniRef(String uniRef)
 	{
 		this.uniRef = uniRef;
 	}

 	public void setCustRef(String custRef)
 	{
 		this.custRef = custRef;
 	}

 	public void setDayHireRate(double dayHireRate)
 	{
 		this.dayHireRate = dayHireRate;
 	}

 	public void setStartDate(Calendar startDate)
 	{
 		this.startDate = startDate;
 	}

 	public void setEndDate(Calendar endDate)
 	{
 		this.endDate = endDate;
 	}

 	public void setTentHired(Tent tentHired)
 	{
 		this.tentHired = tentHired;
 	}

 		//get Methods
 	public String getUniRef()
 	{
 		return uniRef;
 	}

 	public String getCustRef()
 	{
 		return custRef;
 	}

 	public double getDayHireRate()
 	{
 		return dayHireRate;
 	}

 	public Calendar getStartDate()
 	{
 		return startDate;
 	}

 	public Calendar getEndDate()
 	{
 		return endDate;
 	}

 	public Tent getTentHired()
 	{
 		return tentHired;
 	}

		//hired status of tent
 	public void setTent(Tent tentHired)
 	{
 		this.tentHired = tentHired;

 			//hire status updated
		tentHired.setAvailability(false);
 	}

 	public void terminateHire(Calendar endDate)
 	{
 			//end date updated, returnTent method called to end rental
 		this.endDate = endDate;
		tentHired.returnTent(startDate, endDate);

			//tent object updated with most recent rental
 		tentHired.setRecentContract(this);
 	}

 	public double getTotalCost(Calendar startDate, Calendar endDate)
 	{
 			//variable
 		double hireCost;

			//calendar object created using getInstance() method
 		Calendar today = Calendar.getInstance();

 		long t1 = startDate.getTimeInMillis();
 		long t2 = endDate.getTimeInMillis();
 		long totalDays;

 		totalDays = t2 - t1;

			//total from milliseconds to days
 		totalDays = totalDays/1000/60/60/60/24;

 		hireCost = totalDays * dayHireRate;

 		return hireCost;
 	}

 		//output HireContract data and latest reference number of Tent object
 	public String toString()
 	{
 			//variables
 		String tentRef;
 		tentRef = tentHired.getRef();

 		String s = "";

		s = s + "**********HIRE CONTRACT INFORMATION**********";
 		s = s + "Unique reference: " + uniRef + "\n";
 		s = s + "Customer reference: " + custRef + "\n";
 		s = s + "Hire price / day: " + dayHireRate + "\n";
 		s = s + "Start date of hire: " + startDate + "\n";
 		s = s + "End date of hire: " + endDate + "\n";
 		s = s + "Tent hire: " + tentHired + "\n";

 		s = s + "**********OTHER INFORMATION**********";
 		s = s + "Tent reference: " + tentRef + "\n";

 		return s;
 	}
}